export interface Participante {
    matricula: string;
    sigla: string;
    nascimento: Date;
    id: string;
}
