import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ParticipanteService {

  constructor(
    private http: HttpClient
  ) { }

  getParticipantes() {
    return new Promise((resolve, reject) => {
      this.http.get('http://localhost:3000/participantes').subscribe(response => {
        resolve(response);
      });
    });
  }
}
